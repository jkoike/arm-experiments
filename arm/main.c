#include <sys/stat.h>

typedef __caddr_t caddr_t;
 
enum {
 UART_FR_RXFE = 0x10,
 UART_FR_TXFF = 0x20,
 UART0_ADDR = 0x10009000,
};
 
#define UART_DR(baseaddr) (*(unsigned int *)(baseaddr))
#define UART_FR(baseaddr) (*(((unsigned int *)(baseaddr))+6))
#define pchar(c) UART_DR(UART0_ADDR) = c;
 
int read(char *ptr, int len) {
 int todo;
 if(len == 0)
  return 0;
 while(UART_FR(UART0_ADDR) & UART_FR_RXFE);
 *ptr++ = UART_DR(UART0_ADDR);
 for(todo = 1; todo < len; todo++) {
  if(UART_FR(UART0_ADDR) & UART_FR_RXFE) {
   break;
 }
 *ptr++ = UART_DR(UART0_ADDR);
 }
 return todo;
}

void show_int(unsigned char c){
  pchar('0');
  pchar('x');
  unsigned char i;
  i = (c | 15) >> 4;
  if(i > 9){
    pchar('A' + (i - 10));
  } else pchar('0' + i);
  i = c & 15;
  if(i > 9){
    pchar('A' + (i - 10));
  } else pchar('0' + i);
  pchar('\n');
}

void print(char* c){
  while(*c)
    pchar(*c++);
}

void c_entry(){
  char input;
  print("Hello, world!\n");
  while(read(&input, 1)){
    if(input == 0x03) exit();
    show_int(input);
  }
}
