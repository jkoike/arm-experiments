arm: arm/main.c arm/main.h arm/main.ld arm/main.s
	arm-linux-gnueabi-as     -mcpu=cortex-a8 -g arm/main.s -o out/main.o
	arm-linux-gnueabi-gcc -c -mcpu=cortex-a8 -g arm/main.c -o out/main_c.o
	arm-linux-gnueabi-ld -T arm/main.ld out/main.o out/main_c.o -o out/main.elf
	arm-linux-gnueabi-objcopy -O binary out/main.elf out/main.bin

test: arm
	qemu-system-arm -M realview-pb-a8 -m 128M -nographic -kernel out/main.bin

test_dbg: arm
	qemu-system-arm -s -S -M realview-pb-a8 -m 128M -nographic -kernel out/main.bin
